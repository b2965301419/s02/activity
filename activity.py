year = int(input("Please input a year \n"))
if year % 4 == 0:
	print(f"{year} is a leap year")
else:
	print(f"{year} is not a leap year")

row = int(input("Enter number of rows \n"))
col = int(input("Enter number of columns \n"))

i = 1
j = 1
str = ""
while i <= row:
    while j <= col:
        str += "*"
        j += 1
    print(str)
    i += 1
